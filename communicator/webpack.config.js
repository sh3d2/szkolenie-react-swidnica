/* eslint-disable no-var, strict, prefer-arrow-callback */
'use strict';

var path = require('path');
var webpack = require('webpack');
var CommonsChunkPlugin = require("./node_modules/webpack/lib/optimize/CommonsChunkPlugin");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var autoprefixer = require('autoprefixer');

module.exports = {
    cache: true,
    entry: [
        './src/main.tsx'
    ],
    output: {
        path: path.resolve(__dirname, './dist/scripts'),
        filename: '[name].js',
        libraryTarget: "amd",
        chunkFilename: '[chunkhash].js',
        library: "wmc-gui"
    },
    externals: {
        libwmc: "libwmc"
    },
    module: {
        loaders: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                loader: 'babel-loader?presets[]=es2015,presets[]=react!ts-loader'
            },
            {
                test: /(\.js|\.jsx)$/,
                exclude: /(node_modules)/,
                loader: 'babel',
                query: {
                    presets:['es2015','react']
                }
            },
            {
                test: /\.(scss|css)$/,
                loader: ExtractTextPlugin.extract('style', 'css?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss!sass')
            },
            {
                test: /\.json$/,
                loader: 'json'
            },
            {
                test: /\.svg$/,
                loader: "url-loader?limit=10000&mimetype=image/svg+xml"
            }
        ]
    },
    postcss: [ autoprefixer ],
    sassLoader: {
        data: '@import "' + path.resolve(__dirname, 'src/theme.scss') + '";'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new CommonsChunkPlugin("commons.js"),
        new ExtractTextPlugin("[name].css")
    ],
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.jsx', '.scss', '.json'],
        modulesDirectories: [
            'node_modules',
            path.resolve(__dirname, './node_modules')
        ],
        alias: {
            config:path.join(__dirname, 'config', process.env.ENV || 'local'),
        }
    }
};
