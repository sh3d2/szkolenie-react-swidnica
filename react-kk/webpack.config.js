var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');

module.exports = {
    context: __dirname,
    entry: "./script.js",
    output: {
        path: path.join( __dirname, 'dist/'),
        filename: "dist.js"
    },
    presets: [
        "stage-0"
    ],
    module: {
        loaders: [
            { test: /\.js|\.jsx/, loader: "babel", exclude: /node_modules/  },
            { test: /\.css$/, 
              loader: ExtractTextPlugin.extract('style', ['css']),
            }
        ]   
    },
    // Use the plugin to specify the resulting filename (and add needed behavior to the compiler)
    plugins: [
        new ExtractTextPlugin("styles.css"),
        // new webpack.optimize.UglifyJsPlugin({
        //     compress: {
        //         warnings: false
        //     }
        // }),
        new HtmlWebpackPlugin({
            template: 'index.html',
            minify: {
                html5: true
            }
        })
    ],
    devtool: 'source-map'
};