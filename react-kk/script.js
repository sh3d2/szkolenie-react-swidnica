import React from 'react';
import ReactDOM from 'react-dom';

import UserList from './src/components/UserList';


var users = [
    {
        id:1,
        name: 'zenek'
    },
    {
        id:2,
        name: 'heniek'
    },
    {
        id:3,
        name: 'benek'
    },
    {
        id:4,
        name: 'dupek'
    }
];



class App extends React.Component{
    render() {
        return (<UserList data={users} />)
    }
}

ReactDOM.render(<App/>, document.getElementById('app'));