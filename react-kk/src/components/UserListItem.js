
import React from 'react';

class UserListItem extends React.Component{

    render(){
        return (<li key={this.props.user.name} >{this.props.user.id}:{this.props.user.name}</li>)
    }
}

export default UserListItem;