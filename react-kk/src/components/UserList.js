
import React from 'react';
import UserListItem from './UserListItem';

class UserList extends React.Component{

    render(){
        return (<ul>
            {this.props.data.map((el) => <UserListItem key={el.id} user={el}/>)}
        </ul>)
    }


}

export default UserList;